# Hoja de Trucos de Markdown

## Encabezados
################################################################

# Encabezado de primer nivel
## Encabezado de segundo nivel
### Encabezado de tercer nivel
#### Encabezado de cuarto nivel
##### Encabezado de quinto nivel

################################################################

## Listas
### Lista desordenada
################################################################

- Elemento 1
- Elemento 2
    - Elemento 2.1
    - Elemento 2.2
- Elemento 3

################################################################

### Lista ordenada
################################################################

1. Elemento 1
2. Elemento 2
3. Elemento 3
    1. Elemento 3.1
    2. Elemento 3.2

################################################################

## Enlaces
[Texto del enlace](URL)

(!Kratos](image.png))

## Imágenes
![Texto alternativo](URL de la imagen)

![Kratos](image.png)

(![DayZ](image-1.png))

### Alineamientos
<p align="center">
<img src="https://images.unsplash.com/photo-1415604934674-561df9abf539?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2772&q=80" width="100" height="100" border="10"/>
</p>

## Énfasis
### Cursiva
################################################################

#### Tipo #1:
*Texto en cursiva*

#### Tipo #2:
_Texto en negrita_

################################################################

### Negrita
################################################################

#### Tipo #1:
**Texto en negrita**

#### Tipo #2:
__Texto en negrita__

################################################################

## Stilos de texto
################################################################
Bold

**El gran perro salto encima mio.**

__El gran perro salto encima mio.__

<strong>El gran perro salto encima mio.</strong>

Italic

*El gran perro salto encima mio.*

_El gran perro salto encima mio._

<em>El gran perro salto encima mio.</em>

Bold and Italic

**_El gran perro salto encima mio._**

<strong><em>El gran perro salto encima mio.</em></strong>
################################################################

### Monoespacio
<samp>The quick brown fox jumps over the lazy dog.</samp>

### Linea baja
<ins>The quick brown fox jumps over the lazy dog.</ins>

### Tachado
~~The quick brown fox jumps over the lazy dog.~~

## Citas

> Esto es una cita

## Caja
<table><tr><td>The quick brown fox jumps over the lazy dog.</td></tr></table>

## Tablas

| Encabezado 1 | Encabezado 2 | Encabezado 3 |
|------------- |--------------|--------------|
| Celda 1.1    | Celda 1.2    | Celda 1.3    |
| Celda 2.1    | Celda 2.2    | Celda 2.3    |
| Celda 3.1    | Celda 3.2    | Celda 3.3    |

## Código
public static String monthNames[] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

### Bloque de código diferencial
- this code or text is the old version
+ this is what it was changed to

### Comparaciones
<table>
<tr>
<th>Antes</th>
<th>Despues</th>
</tr>
<tr>
<td>
<pre lang="js">
console.log(fullName); // undefined
fullName = "Dariana Trahan";
console.log(fullName); // Dariana Trahan
var fullName;
</pre>
</td>
<td>
<pre lang="js">
var fullName;
console.log(fullName); // undefined
fullName = "Dariana Trahan";
console.log(fullName); // Dariana Trahan
</pre>
</td>
</tr>
</table>

## Completar tareas
- [x] Fix Bug 223
- [ ] Add Feature 33
- [ ] Add unit tests

## Botones
<kbd>ctrl + shift + p</kbd>

## Operaciones matemáticas
$x = {-b \pm \sqrt{b^2-4ac} \over 2a}$




